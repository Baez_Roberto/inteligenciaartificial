package ClasesResultado;

import ClasesArbol.Nodo;
import ClasesDataSet.DataSet;
import ClasesInterfaz.Linea;
import ClasesInterfaz.MiRender;
import ClasesInterfaz.PanelDibujo;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Scanner;

public class Resultado extends JFrame  {

    private JPanel panelResultado;
    private List<Nodo> arbolDecision;
    private JTable tablaPueba;
    private JTable tablaEntrenamiento;
    private JTextField valorEjex;
    private JTextField valorEjey;
    private JTextField valorClase;
    private String clase1="";
    private String clase2="";
    private final JTree tree;

    public Resultado(DefaultTreeModel modelo, String dataSet, List<Linea> lineasUmbral, String[] dataSetPrueba, String[] dataSetEntrenamiento, final List<Nodo> arbolDecision, final String[] clasesDataSet, String separador) throws FileNotFoundException {
        this.arbolDecision = arbolDecision;


        pack();
        setSize(1500, 1500);
        setVisible(true);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        setTitle("Arbol de Decision - "+ dataSet);
        ImageIcon ImageIcon = new ImageIcon(getClass().getResource("/iconoApp.png"));
        Image Image = ImageIcon.getImage();
        setIconImage(Image);

        Scanner scanner = null;
        if (dataSet==null){
            scanner= new Scanner(dataSetEntrenamiento[0]);
        }else{
            scanner = new Scanner(new File(dataSet));
        }

        String registro = scanner.nextLine();
        int cantidadTotal=0;
        while(scanner.hasNextLine()) {
            registro = scanner.nextLine();
            String punto[] = registro.split(separador);
            if (clase1 == "") {
                clase1 = punto[2];
            }
            if (!clase1.equals(punto[2]) && clase2.equals("")) {
                clase2 = punto[2];
            }
        }



        /**Panel donde se muestran los resultados
         de la clasificación
        */
        panelResultado = new JPanel();
        panelResultado.setVisible(true);
        panelResultado.setLayout(null);
        setContentPane(panelResultado);

        /**Se agrega la posibilidad al
        usuario de ingresar un punto y lo clasifique
        */
        JLabel mensajeNuevoPunto = new JLabel("Ingrese punto a clasificar: ");
        mensajeNuevoPunto.setBounds(850, 350, 200,200);
        panelResultado.add(mensajeNuevoPunto);

        JLabel ejeX= new JLabel("X:");
        valorEjex =new JTextField();
        valorEjex.setBounds(870, 470, 85, 20);
        valorEjex.addKeyListener(new KeyAdapter()
        {
            public void keyTyped(KeyEvent e)
            {
                char caracter = e.getKeyChar();
                if(((caracter < '0') || (caracter > '9') ) &&  (caracter != '.')&&  (caracter != '-')){
                    e.consume();
                }
            }
        });
        ejeX.setBounds(850, 470, 85, 23);


        final JLabel ejeY = new JLabel("Y:");
        valorEjey =new JTextField();
        valorEjey.setBounds(870, 500, 85, 20);
        valorEjey.addKeyListener(new KeyAdapter()
        {
            public void keyTyped(KeyEvent e)
            {
                char caracter = e.getKeyChar();
                if(((caracter < '0') || (caracter > '9') ) &&  (caracter != '.')&&  (caracter != '-') ){
                    e.consume();
                }
            }
        });
        ejeY.setBounds(850, 500, 85, 23);

        panelResultado.add(ejeX);
        panelResultado.add(valorEjex);
        panelResultado.add(valorEjey);
       panelResultado.add(ejeY);

        JLabel claseResultado = new JLabel("Clase: ");
        valorClase =new JTextField();
        valorClase.setBounds(1050, 470, 180, 20);
        valorClase.setEditable(false);
        claseResultado.setBounds(1000, 470, 85, 23);

        panelResultado.add(claseResultado);
        panelResultado.add(valorClase);

        JButton btnAgregarPunto = new JButton("Clasificar Punto");
        btnAgregarPunto.setBounds(1000, 560, 200, 20);
        panelResultado.add(btnAgregarPunto);


        btnAgregarPunto.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                if (!(isNumeric(valorEjex.getText())) ||!(isNumeric(valorEjey.getText())) ){
                    JOptionPane.showMessageDialog(null,"Valores ingresados no numericos");
                }else{
                    AnalizadorPunto punto = new AnalizadorPunto(Double.parseDouble(valorEjex.getText()),Double.parseDouble(valorEjey.getText()), clasesDataSet);
                    valorClase.setText(punto.clasificarPunto(arbolDecision.get(0)));
                }
            }
        });

        JButton btnExpandirArbol = new JButton("Expandir");
        btnExpandirArbol.setBounds(20, 560, 100, 20);
        panelResultado.add(btnExpandirArbol);

        JButton btnContraerArbol = new JButton("Contraer");
        btnContraerArbol.setBounds(140, 560, 100, 20);
        panelResultado.add(btnContraerArbol);

        tree = new JTree(modelo);
        tree.setSize(0,0);
        tree.setCellRenderer(new MiRender(DataSet.getClasesDataSet()));
        JScrollPane scrollArbol =  new JScrollPane(tree);
        panelResultado.add(scrollArbol);
        scrollArbol.setBounds(20,20,250,500);


        btnExpandirArbol.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                expandirTodo();
            }
        });

        btnContraerArbol.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                cerrarTodo(tree);
            }
        });


        /**Crea el eje cartesiano con los puntos
        que se usaron para el entrenamiento del data set
        */
        PanelDibujo pd;
        if (dataSetEntrenamiento!=null){
             pd = new PanelDibujo(null,lineasUmbral,dataSetEntrenamiento,separador);
        }else{
             pd = new PanelDibujo(dataSet,lineasUmbral,dataSetEntrenamiento,separador);
        }

        JScrollPane scrollEje = new JScrollPane(pd.getContent());
        scrollEje.setBounds(320,20,500,500);
        scrollEje.getViewport().setViewPosition(new Point(250,250));
        panelResultado.add(pd.getControl(),"last");
        panelResultado.add(scrollEje);

        /**Se muestra de que color
        es cada clase en el grafico
        */
        JLabel lblClase1 = new JLabel("Clase: " + clase2);
        lblClase1.setBounds(373,575,150,30);
        JTextField colorClase1 =new JTextField();
        colorClase1.setBounds(350, 580, 20, 20);
        colorClase1.setBackground(new Color(76, 214, 234));
        colorClase1.setEditable(false);
        colorClase1.setVisible(true);
        panelResultado.add(colorClase1);
        panelResultado.add(lblClase1);

        JLabel lblClase2 = new JLabel("Clase: " + clase1);
        lblClase2.setBounds(373,600,150,30);
        JTextField colorClase2 =new JTextField();
        colorClase2.setBounds(350, 605, 20, 20);
        colorClase2.setBackground(new Color(234, 114, 181));
        colorClase2.setEditable(false);
        colorClase2.setVisible(true);
        panelResultado.add(colorClase2);
        panelResultado.add(lblClase2);

        /**Muestro el data set que se uso
         para entrenar el arbol
        */
        JLabel lblEntrenamiento = new JLabel("Data set para Entrenamiento: ");
        lblEntrenamiento.setBounds(850, 20, 250, 30);
        panelResultado.add(lblEntrenamiento);
        tablaEntrenamiento = new JTable();
        tablaEntrenamiento.setBounds(850, 45, 500, 150);
        tablaEntrenamiento.setVisible(true);

        Scanner scanE;
        if (dataSetEntrenamiento==null){
            scanE = new Scanner(new File(dataSet));
        }else{
            scanE = new Scanner(dataSetEntrenamiento[0]);
        }

        String headerLineE = scanE.nextLine();
        String headersE[]  = headerLineE.split(separador);
        DefaultTableModel modeloTablaE= new DefaultTableModel(null,headersE);
        while (scanE.hasNext()){
            String registroTabla = scanE.nextLine();
            String registroSinComas [] = registroTabla.split(separador);
            modeloTablaE.addRow(registroSinComas);
        }

        tablaEntrenamiento.setModel(modeloTablaE);
        JScrollPane scrollTablaE= new JScrollPane(tablaEntrenamiento);
        scrollTablaE.setBounds(850, 45, 500, 150);
        panelResultado.add(scrollTablaE);


        /**Se muestra el data set de prueba, con el que
        vamos a validar la eficiencia de nuestra clasificacion
        */
        JLabel lblPrueba = new JLabel("Data set para Test: ");
        lblPrueba.setBounds(850, 200, 200, 30);
        panelResultado.add(lblPrueba);
        DefaultTableModel modeloTabla;
        tablaPueba = new JTable();
        int cantidadBien=0;
        Double porcentajesBien = 0.0;
        if (dataSetPrueba==null){
            String headers[]  = headerLineE.split(separador);
            modeloTabla= new DefaultTableModel( null,headers);
            modeloTabla.addColumn("Clase Sugerida", (Object[]) null);
        }else{
            tablaPueba.setBounds(850, 225, 500, 150);
            tablaPueba.setVisible(true);
            Scanner scan = new Scanner(dataSetPrueba[0]);
            String headerLine = scan.nextLine();
            String headers[]  = headerLine.split(separador);
            modeloTabla= new DefaultTableModel(null,headers);
            modeloTabla.addColumn("Clase Sugerida", (Object[]) null);
            while (scan.hasNext()){
                cantidadTotal++;
                String registroTabla = scan.nextLine();
                String registroSinComas [] = registroTabla.split(separador);
                AnalizadorPunto punto = new AnalizadorPunto(Double.parseDouble(registroSinComas[0]),Double.parseDouble(registroSinComas[1]),clasesDataSet);
                String claseSugerida[] = new String[]{punto.clasificarPunto(arbolDecision.get(0))};
                if((registroSinComas[2].equals(claseSugerida[0]))){
                    cantidadBien++;
                }
                String[] resultado= new String[registroSinComas.length+claseSugerida.length];
                System.arraycopy(registroSinComas, 0, resultado, 0, registroSinComas.length);
                System.arraycopy(claseSugerida, 0, resultado, registroSinComas.length, claseSugerida.length);
                modeloTabla.addRow(resultado);
            }
             porcentajesBien = (double)(cantidadBien*100)/(cantidadTotal);
        }

        tablaPueba.setModel(modeloTabla);
        JScrollPane scrollTabla = new JScrollPane(tablaPueba);
        scrollTabla.setBounds(850, 225, 500, 150);
        panelResultado.add(scrollTabla);

        
        JLabel porcentaje = new JLabel("El porcentaje de acierto en la clasificación es: %" + porcentajesBien.toString() );
        porcentaje.setBounds(850,150,500,500);
        if (dataSetEntrenamiento!=null){
            panelResultado.add(porcentaje);
        }
    }

    /**
     * Metodo que devuelve verdadero si el valor ingresado por el usuario es un numero
     * @param cadena
     * @return
     */

    public boolean isNumeric(String cadena) {

        boolean resultado;

        try {
            Double.parseDouble(cadena);
            resultado = true;
        } catch (NumberFormatException excepcion) {
            resultado = false;
        }

        return resultado;
    }



    public void cerrarTodo(JTree tree) {
        TreePath pathToRoot = new TreePath(tree.getModel().getRoot());
        cerrarPaths(tree, pathToRoot);
        if (!tree.isRootVisible())
            tree.expandPath(pathToRoot);
    }

    public void cerrarPaths(JTree tree, TreePath path) {
        Object node = path.getLastPathComponent();
        TreeModel model = tree.getModel();
        if (model.isLeaf(node))
            return;
        int num = model.getChildCount(node);
        for (int i = 0; i < num; i++)
            cerrarPaths(tree, path.pathByAddingChild(model.getChild(node, i)));
        tree.collapsePath(path);
    }

    protected void expandirPaths(TreePath path) {
        tree.expandPath(path);
        final Object node = path.getLastPathComponent();
        final int n = tree.getModel().getChildCount(node);
        for (int index = 0; index < n; index++) {
            final Object child = tree.getModel().getChild(node, index);
            expandirPaths(path.pathByAddingChild(child));
        }
    }


    public void expandirTodo() {
        final Object root = tree.getModel().getRoot();
        if (root != null) {
            expandirPaths(new TreePath(root));
        }
    }


}
