package ClasesResultado;

import ClasesArbol.Nodo;

/**
 * Esta clase se utiliza para clasificar un punto nuevo ingresado por el usuario
 * según el arbol generado
 */

public class AnalizadorPunto {
    private String[] clasesDataSet;
    private Double x;
    private Double y;

    /**
     * Constructor de la clase, recibe como parametro las coordenadas del punto
     * y las clases del data set
     * @param x
     * @param y
     * @param clasesDataSet
     */
    public AnalizadorPunto(Double x, Double y, String[] clasesDataSet){

        this.x=x;
        this.y=y;
        this.clasesDataSet = clasesDataSet;
    }

    /**
     * Metodo que clasifica el punto
     * @param nodo
     * @return
     */
    public String clasificarPunto(Nodo nodo) {
        Double umbral = 0.0;
        int valorMenor = 0;
        int valorMayor = 0;
        for (int indexArbol = 0; indexArbol < nodo.getRamas().size(); indexArbol++) {
            if (nodo.getRamas().get(indexArbol).getNombreValor().contains("<")) {
                umbral = Double.parseDouble(nodo.getRamas().get(indexArbol).getNombreValor().split("<")[1]);
                valorMenor = indexArbol;
            } else {
                umbral = Double.parseDouble(nodo.getRamas().get(indexArbol).getNombreValor().split(">=")[1]);
                valorMayor = indexArbol;
            }
        }

        String resultado;
        if (nodo.getNombre().equals(clasesDataSet[0])) {
            if (x <= umbral) {
                if (nodo.getRamas().get(valorMenor).getNodos() == null) {
                    return nodo.getRamas().get(valorMenor).getHoja();
                } else {
                    resultado = this.clasificarPunto(nodo.getRamas().get(valorMenor).getNodos());
                }
            } else {
                if (nodo.getRamas().get(valorMayor).getNodos() == null) {
                    return nodo.getRamas().get(valorMayor).getHoja();
                } else {
                    resultado = this.clasificarPunto(nodo.getRamas().get(valorMayor).getNodos());
                }
            }
        } else {
            if (y <= umbral) {
                if (nodo.getRamas().get(valorMenor).getNodos() == null) {
                    return nodo.getRamas().get(valorMenor).getHoja();
                } else {
                    resultado = clasificarPunto(nodo.getRamas().get(valorMenor).getNodos());
                }
            } else {
                if (nodo.getRamas().get(valorMayor).getNodos() == null) {
                    return nodo.getRamas().get(valorMayor).getHoja();
                } else {
                    resultado = this.clasificarPunto(nodo.getRamas().get(valorMayor).getNodos());
                }
            }
        }
        return resultado;
    }

    public Double getX() {
        return x;
    }

    public void setX(Double x) {
        this.x = x;
    }

    public Double getY() {
        return y;
    }

    public void setY(Double y) {
        this.y = y;
    }

}
