package ClasesArbol;

import ClasesParticion.Particion;

import java.util.ArrayList;
import java.util.List;

/**
 * Clase que representa el nodo que se dibujara en la el arbol, que contiene el eje por donde se va
 * a particionar y las dos ramas por menor y mayor igual al umbral
 */
public class Nodo {

    private List<Rama> ramas = new ArrayList<Rama>();
    private String nombre;



    public Nodo(String nombre, List<Particion> particiones, Double mejorumbral) {
        this.nombre = nombre;
        /**
         * Instanciamos las ramas y las asociamos al nodo.
         */
        for (int x=0;x < particiones.size();x++ ){
           Rama rama = new Rama(particiones.get(x).getNombreParticion(),particiones.get(x).esPuro(), particiones.get(x));
            this.ramas.add(rama);
        }
    }




    public List<Rama> getRamas() {
        return ramas;
    }



    public boolean esPuro() {
        int contadorRamasPuras = 0;
        for (Rama rama: ramas){
            if (rama.isPureza()){
                contadorRamasPuras++;
            }
        }
        if(contadorRamasPuras==ramas.size()){
            return true;
        }
        return false;
    }

    public String getNombre() {
        return nombre;
    }
}
