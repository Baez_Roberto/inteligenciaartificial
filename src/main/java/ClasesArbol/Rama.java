package ClasesArbol;

import ClasesParticion.Particion;

/**
 * Clase que representa la rama del nodo, almacena
 * su particion y en caso de ser pura tiene su rama
 */
public class Rama {
    private String nombreValor;
    private Nodo nodo;
    private String hoja;
    private Particion particion;
    private boolean pureza;


    /**
     *  Constructor de la clase Rama
     * @param nombreParticion
     * @param purezaParticion
     * @param particion
     */
    public Rama(String nombreParticion, boolean purezaParticion, Particion particion) {
        this.nombreValor=nombreParticion;
        this.pureza=purezaParticion;
        this.particion=particion;
    }

    /**
     * Getters y Setters
     * @return
     */

    public String getNombreValor() {
        return nombreValor;
    }

    public void setNombreValor(String nombreValor) {
        this.nombreValor = nombreValor;
    }

    public boolean isPureza() {
        return pureza;
    }

    public void setPureza(boolean pureza) {
        this.pureza = pureza;
    }

    public Nodo getNodos() {
        return nodo;
    }

    public void setNodos(Nodo nodo) {
        this.nodo = nodo;
    }

    public String getHoja() {
        return hoja;
    }

    public void setHoja(String hoja) {
        this.hoja = hoja;
    }

    public Particion getParticion() {
        return particion;
    }

    public void setParticion(Particion particion) {
        this.particion = particion;
    }
}
