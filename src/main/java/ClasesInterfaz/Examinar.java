package ClasesInterfaz;

import ClasesDataSet.DataSet;
import ClasesResultado.Resultado;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

/**
 * Clase que genera la interfaz de usuario principal, de esta interfaz se seleeciona el archivo y se carga el
 * porcentaje que se quiere considerar para el data set de pruebas
 */

public class Examinar extends JFrame implements  ActionListener, ChangeListener {

    private JPanel contentPane;
    private JTextField textField;
    private JTextField valorPorcentaje;
    private JTextField valorSeperador;
    private JTable tabla;
    private JProgressBar barra;
    private JCheckBox porcentaje;
    private JCheckBox separador;
    private JFileChooser fc=new JFileChooser();
    private int n =0;
    private Timer t;
    private String[] expresionesRegulares = new String[]{ "$", "^",  "*", "+", "?", "[", "]","." };




    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    Examinar frame = new Examinar();
                    ImageIcon ImageIcon = new ImageIcon(getClass().getResource("/iconoApp.png"));
                    Image Image = ImageIcon.getImage();
                    frame.setIconImage(Image);
                    frame.setVisible(true);
                    frame.setTitle("Arboles de Decisión - Atributos Continuos");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the frame.
     */
    public Examinar() {


        FileNameExtensionFilter filtro1 = new FileNameExtensionFilter("*.txt", "txt");
        FileNameExtensionFilter filtro2 = new FileNameExtensionFilter("*.csv", "csv");
        fc.setFileFilter(filtro1);
        fc.setFileFilter(filtro2);

        //Parametros asociados a la ventana
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(200, 200, 450, 350);
        contentPane = new JPanel();
        contentPane.setLayout(null);
        setContentPane(contentPane);

        textField = new JTextField();
        textField.setToolTipText("Insertar la ruta del data set");
        textField.setBounds(52, 26, 209, 20);
        contentPane.add(textField);
        textField.setColumns(10);

        JButton btnSeleccionar = new JButton("Seleccionar");
        btnSeleccionar.setToolTipText("Seleccionar data set");
        btnSeleccionar.setBounds(288, 25, 140, 23);
        contentPane.add(btnSeleccionar);

        JButton btnGenerarArbol = new JButton("Generar Arbol");
        btnGenerarArbol.setToolTipText("Genera árbol de decisión");
        btnGenerarArbol.setBounds(288, 245, 140, 23);
        contentPane.add(btnGenerarArbol);

        tabla = new JTable();
        tabla.setBounds(52,76,360,156);


        JScrollPane scroll=new JScrollPane(tabla);
        scroll.setBounds(52, 76, 360, 156);
        contentPane.add(scroll);


        porcentaje=new JCheckBox("Data set Prueba: %",false);
        porcentaje.setToolTipText("Particionar data set en Entrenamiento y Prueba");
        porcentaje.setBounds(52, 245, 165, 23);
        porcentaje.addChangeListener(this);
        contentPane.add(porcentaje);

        separador = new JCheckBox("Separador data set:", false);
        separador.setToolTipText("Separador data set");
        separador.setBounds(52, 50, 165, 23);
        separador.addChangeListener(this);
        contentPane.add(separador);

        valorPorcentaje= new JTextField();
        valorPorcentaje.setEnabled(false);
        valorPorcentaje.setToolTipText("Porcentaje que se considerará para probar clasificación");
        valorPorcentaje.setText("0");
        valorPorcentaje.addKeyListener(new KeyAdapter()
        {
            public void keyTyped(KeyEvent e)
            {
                char caracter = e.getKeyChar();
                if(((caracter < '0') || (caracter > '9') || Integer.parseInt(valorPorcentaje.getText()+caracter)>99 ) ){
                    e.consume();
                }
            }
        });
        valorPorcentaje.setBounds(218, 245, 29, 23);
        contentPane.add(valorPorcentaje);

        valorSeperador= new JTextField();
        valorSeperador.setEnabled(false);
        valorSeperador.setToolTipText("Porcentaje que se considerará para probar clasificación");
        valorSeperador.setText(";");
        valorSeperador.setBounds(218, 50, 29, 23);


        contentPane.add(valorSeperador);





        barra = new JProgressBar(0, 100);
        barra.setBounds(5, 10, 280, 25);
        barra.setStringPainted(true);

        ActionListener accion=new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                if(n<=100){
                    barra.setValue(n);
                    n=n+10;
                }else{
                    t.stop();
                    barra.setString("Termino");
                }
            }
        };

        t =new Timer(500, accion);


        JPanel panelBarra = new JPanel();
        panelBarra.setLayout(null);
        panelBarra.add(barra);

        btnGenerarArbol.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {

                int opcionPorcentaje = 0;
                if(valorPorcentaje.getText().equals("")){
                    valorPorcentaje.setText("0");
                }

                for (String er: expresionesRegulares){
                    if (valorSeperador.getText().equals(er)){
                        valorSeperador.setText("\\" + er);
                    }
                }

                if (Integer.parseInt(valorPorcentaje.getText())>20 && !(textField.getText().equals(""))){
                    String aviso="El porcentaje para prueba ingresado es muy alto,la clasificación no sera eficiente." + '\n' +"Se sugiere un porcentaje menor al %20" + '\n'+ "¿Desea continuar?";
                    opcionPorcentaje = JOptionPane.showConfirmDialog(null,aviso,"Confirmar Porcentaje",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
                }

                if (textField.getText().equals("")){
                    JOptionPane.showMessageDialog(null,"Debe seleccionar un Data Set");
                }else{
                    if(opcionPorcentaje == 0){
                        DataSet dataSet = new DataSet();
                        boolean ok  =dataSet.generarArbol(textField.getText(), valorPorcentaje.getText(),valorSeperador.getText());
                        try {
                            if (ok){
                                Resultado resultado = new Resultado(dataSet.getModelo(),textField.getText(),dataSet.getLineasUmbral(),
                                        dataSet.getDataSetPrueba(),dataSet.getDataSetEntrenamiento(),dataSet.getArbolDecision(),dataSet.getClasesDataSet(),dataSet.getSeparador());
                            }
                        } catch (FileNotFoundException e1) {
                            JOptionPane.showMessageDialog(null,"No se encuentra archivo en la ruta indicada");
                        }

                    }
                }



            }
        });

        btnSeleccionar.addActionListener(new ActionListener(){
            public void actionPerformed (ActionEvent e){

                for (String er: expresionesRegulares){
                    if (valorSeperador.getText().equals(er)){
                        valorSeperador.setText("\\" + er);
                    }
                }

                fc.setAcceptAllFileFilterUsed(false);



                int seleccion=fc.showOpenDialog(contentPane);
                if(seleccion==JFileChooser.APPROVE_OPTION){
                    File fichero=fc.getSelectedFile();
                    fc.setCurrentDirectory(fc.getSelectedFile());
                    try(FileReader fr=new FileReader(fichero)){

                        Scanner scan = new Scanner(new File(fichero.getAbsolutePath()));
                        String headerLine = scan.nextLine();
                        String headers[]  = headerLine.split(valorSeperador.getText());
                        if (headers.length!=3){
                            JOptionPane.showMessageDialog(null,"El formato del archivo no es el esperado");
                            textField.setText("");
                        }else{
                            DefaultTableModel modelo= new DefaultTableModel(null,headers);
                            while (scan.hasNext()){
                                String registro = scan.nextLine();
                                String registroSinComas [] = registro.split(valorSeperador.getText());
                                if (registroSinComas.length!=3){
                                    JOptionPane.showMessageDialog(null,"El formato del archivo no es el esperado");
                                    textField.setText("");
                                    modelo=null;
                                    break;
                                }else{
                                    boolean esNumero=true;
                                    for (int x=0;x < registroSinComas.length - 1;x++){
                                        if (!(isNumeric(registroSinComas[x]))){
                                            JOptionPane.showMessageDialog(null,"El formato del archivo no es el esperado");
                                            esNumero=false;
                                            break;
                                        }
                                    }
                                    if (esNumero){
                                        modelo.addRow(registroSinComas);
                                    }else{
                                        modelo.setNumRows(0);
                                        modelo.setColumnCount(0);
                                        textField.setText("");
                                        fichero =null;
                                        break;
                                    }
                                }
                            }
                            if (fichero!=null){
                                textField.setText(fichero.getAbsolutePath());
                            }
                            tabla.setModel(modelo);
                        }

                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        });

    }


    public boolean isNumeric(String cadena) {

        boolean resultado;

        try {
            Double.parseDouble(cadena);
            resultado = true;
        } catch (NumberFormatException excepcion) {
            resultado = false;
        }

        return resultado;
    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }


    public void stateChanged(ChangeEvent e) {
        if (separador.isSelected()) {
            valorSeperador.setEnabled(true);
        } else {
            valorSeperador.setText(";");
            valorSeperador.setEnabled(false);
        }

        if (porcentaje.isSelected()) {
            valorPorcentaje.setEnabled(true);
        } else {
            valorPorcentaje.setText("0");
            valorPorcentaje.setEnabled(false);
        }



    }
}