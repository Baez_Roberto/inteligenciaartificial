package ClasesInterfaz;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Scanner;


public class PanelDibujo extends JPanel implements ChangeListener,MouseMotionListener, MouseWheelListener {
    private String dataSet;
    private String [] dataSetEntrenamiento;
    private List<Linea> umbrales;
    private double zoom = 1;
    public static int x = 0;
    public static int y = 0;
    private float scale = 1;
    private String separador;
    private Plano plano;
    private  BufferedImage image;
    private JLabel label;

    public PanelDibujo(String dataSet, List<Linea> umbrales, String[] dataSetEntrenamiento, String separador){
        this.dataSetEntrenamiento=dataSetEntrenamiento;
        this.dataSet = dataSet;
        this.umbrales=umbrales;
        this.separador=separador;


        addMouseWheelListener(new MouseAdapter() {

            @Override
            public void mouseWheelMoved(MouseWheelEvent e) {
                double delta = 0.05f * e.getPreciseWheelRotation();
                scale += delta;
                revalidate();
                repaint();
            }

        });

    }


    public void createAnImage(){
        int type = BufferedImage.TYPE_INT_ARGB;
        image = new BufferedImage(1000, 1000, type);
        Graphics2D g2 = image.createGraphics();
        g2.setPaint(new Color(251, 249, 255));
        AffineTransform at = new AffineTransform();
        at.scale(zoom, zoom);
        Plano plano = new Plano(1000,1000,at);
        plano.dibujar(g2);




        Scanner scanner = null;
        try {


            if(dataSet==null){
                scanner = new Scanner(dataSetEntrenamiento[0]);
            }else{
                scanner = new Scanner(new File(this.dataSet));
            }
            String clase ="";
            String registro = scanner.nextLine();
            while(scanner.hasNextLine()){
                registro = scanner.nextLine();
                String punto [] = registro.split(separador);
                if(clase==""){
                    clase = punto[2];
                }
                Linea puntoADibujar;
                if (clase.equals(punto[2])){
                    puntoADibujar = new Linea(Double.parseDouble(punto[0]),Double.parseDouble(punto[1]),1);
                }else{
                    puntoADibujar = new Linea(Double.parseDouble(punto[0]),Double.parseDouble(punto[1]),2);
                }
                puntoADibujar.dibujar(g2);


            }

            for (int indexUmbrales=0; indexUmbrales < umbrales.size();indexUmbrales++){
                umbrales.get(indexUmbrales).dibujar(g2);
            }
            g2.dispose();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void mouseDragged(MouseEvent e) {

    }

    @Override
    public void mouseMoved(MouseEvent e) {

    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {

    }

    @Override
    public Dimension getPreferredSize() {
        Dimension size = new Dimension(500, 500);
        if (plano != null) {
            size.width = (int) Math.round(plano.getAlto()* scale);
            size.height = (int) Math.round(plano.getAncho() * scale);
        }
        return size;
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        int value = ((JSlider)e.getSource()).getValue();
        double scale = value/150.0;
        BufferedImage scaled = getScaledImage(scale);
        label.setIcon(new ImageIcon(scaled));
        label.revalidate();  // signal scrollpane
    }

    private BufferedImage getScaledImage(double scale) {
        int w = (int)(scale*image.getWidth());
        int h = (int)(scale*image.getHeight());
        BufferedImage bi = new BufferedImage(w, h, image.getType());
        Graphics2D g2 = bi.createGraphics();
        g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
                RenderingHints.VALUE_INTERPOLATION_BICUBIC);
        AffineTransform at = AffineTransform.getScaleInstance(scale, scale);
        g2.drawRenderedImage(image, at);
        g2.dispose();
        return bi;
    }

    public JSlider getControl() {
        JSlider slider = new JSlider(JSlider.HORIZONTAL, 100, 300, 100);
        slider.setBounds(420,530,300,50);
        slider.setMajorTickSpacing(100);
        slider.setMinorTickSpacing(10);
        slider.setPaintTicks(true);
        slider.setPaintLabels(true);
        slider.addChangeListener(this);
        return slider;
    }

    public JLabel getContent() {
        createAnImage();
        label = new JLabel(new ImageIcon(image));
       // label.setHorizontalAlignment(JLabel.CENTER);
        return label;
    }
}

