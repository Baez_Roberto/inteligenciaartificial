package ClasesInterfaz;

import java.awt.*;
import java.awt.geom.Line2D;


public class Linea {
    private Punto punto1;
    private Punto punto2;
    private int clase;

    public Linea(){}
    /**
     * Constructor de clase.
     * Una linea se forma 2 puntos.
     * @param punto1 Punto (x,y)
     * @param punto2 Punto (x,y)
     */
    public Linea(Punto punto1, Punto punto2){
        this.punto1 = punto1;
        this.punto2 = punto2;

    }

    /**
     * Constructor de clase.
     *
     * @param x1 Punto1 x
     * @param y1 Punto1 y
     * @param x2 Punto2 x
     * @param y2 Punto2 y
     * @param texto Boolean Si quiere que lleve texto con la coordenada.
     */
    public Linea(double x1, double y1, double x2, double y2,boolean texto){
        if(texto){
            this.punto1 = new Punto(x1, y1,true);
            this.punto2 = new Punto(x2, y2,true);
        }else{
            this.punto1 = new Punto(x1, y1,false);
            this.punto2 = new Punto(x2, y2,false);
        }
    }


    public Linea(double x1, double y1,int clase){
        this.clase= clase;
        this.punto1 = new Punto(x1, y1,false);
    }

    /**
     * Metodo dibujar.
     * Dibujara una recta.
     * @param g  Graphics2D
     */
    public void dibujar(Graphics2D g){

        if(punto2==null){
            if (clase == 1){
                g.setPaint(new Color(234, 114, 181));
            }else{
                g.setPaint(new Color(76, 214, 234));
            }

            g.setFont(new Font( "SansSerif", Font.PLAIN, 30 ));
            g.drawString(".", (int)(punto1.get_x()) - 2, (int) punto1.get_y());
        }else{
            g.setPaint(new Color(100,100,100));
            g.setFont(new Font( "SansSerif", Font.PLAIN, 10 ));
            Line2D rectaUmbral = new Line2D.Double(punto1.get_x(), punto1.get_y(),(punto2.get_x()), punto2.get_y());
            g.draw(rectaUmbral);
            g.setColor(Color.BLACK);


        }



    }





}