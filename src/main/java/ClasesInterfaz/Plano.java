package ClasesInterfaz;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Line2D;


public class Plano {
    private double ancho;
    private double alto;



    public Plano(double ancho, double alto, AffineTransform at){
        this.ancho = ancho;
        this.alto = alto;

    }

    public void dibujar(Graphics2D g){


        for(int i = 0; i < alto; i++){
            Line2D  linea = new Line2D.Double(i,0,i,ancho);
            if(i % 25 == 0){
                g.setPaint(new Color(204,204,204));
                g.draw(linea);
            }
        }

        for(int i = 0; i < ancho; i++){
            Line2D  linea = new Line2D.Double(0,i,alto,i);
            if(i % 25 == 0){
                g.setPaint(new Color(204,204,204));
                g.draw(linea);
            }
        }
        g.setPaint(new Color(15, 18, 168));
        Line2D linea_y = new Line2D.Double(ancho/2,0,ancho/2,alto);

        Line2D  linea_x = new Line2D.Double(0,alto/2,ancho,alto/2);
        g.draw(linea_x);
        g.draw(linea_y);
    }

    public double getAncho() {
        return ancho;
    }

    public void setAncho(double ancho) {
        this.ancho = ancho;
    }

    public double getAlto() {
        return alto;
    }

    public void setAlto(double alto) {
        this.alto = alto;
    }



}
