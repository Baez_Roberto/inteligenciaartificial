package ClasesInterfaz;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreeNode;
import java.awt.*;

public class MiRender extends DefaultTreeCellRenderer {


    private final String[] clasesDataSet;

    ImageIcon nodos;

    ImageIcon ramas;

    ImageIcon hoja;

    public MiRender(String[] clasesDataSet) {

        nodos =  new ImageIcon(new ImageIcon(getClass().getResource("/arbol.png")).getImage());

        ramas = new ImageIcon(new ImageIcon(getClass().getResource("/rama.png")).getImage());

        hoja = new ImageIcon(new ImageIcon(getClass().getResource("/hoja.png")).getImage());

        this.clasesDataSet = clasesDataSet;
    }



    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus)

    {

        super.getTreeCellRendererComponent(tree,value,selected,expanded,leaf,row,hasFocus);

        DefaultMutableTreeNode nodo = (DefaultMutableTreeNode)value;

        TreeNode t = nodo;


        if(t.isLeaf()){
            setIcon(hoja);
        } else{

            if (t.getParent()!= null && (t.getParent().toString().equals(clasesDataSet[0])|| t.getParent().toString().equals(clasesDataSet[1]) )){
                setIcon(ramas);
            }else{
                if (t.getParent()==null){
                    setIcon(nodos);
                }else{
                    setIcon(nodos);
                }

            }
        }
        return this;

    }

}
