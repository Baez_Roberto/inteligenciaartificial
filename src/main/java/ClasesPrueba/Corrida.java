package ClasesPrueba;

import ClasesDataSet.DataSet;
import ClasesResultado.AnalizadorPunto;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class Corrida extends JFrame implements ActionListener, ChangeListener {

    private JTextField ruta;
    private JTextArea resultadoCorrida;
    private JTextField valorPorcentaje;
    private JTextField valorSeaprador;
    private JTextField cantidadCorrida;
    private JFileChooser fc=new JFileChooser();
    private JPanel contentPane;
    private JCheckBox porcentaje;
    private JCheckBox separador;

    public Corrida (){
        FileNameExtensionFilter filtro1 = new FileNameExtensionFilter("*.txt", "txt");
        FileNameExtensionFilter filtro2 = new FileNameExtensionFilter("*.csv", "csv");
        fc.setFileFilter(filtro1);
        fc.setFileFilter(filtro2);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(200, 200, 450, 350);
        contentPane = new JPanel();
        contentPane.setLayout(null);
        setContentPane(contentPane);

        ruta = new JTextField();
        ruta.setToolTipText("Inserta la ruta del fichero");
        ruta.setBounds(52, 26, 490, 20);
        contentPane.add(ruta);
        ruta.setColumns(10);

        JButton btnSeleccionar = new JButton("Seleccionar");
        btnSeleccionar.setBounds(550, 25, 140, 23);
        contentPane.add(btnSeleccionar);

        JButton btnComenzarCorrida = new JButton("Empezar Corridas");
        btnComenzarCorrida.setBounds(510, 500, 180, 23);
        contentPane.add(btnComenzarCorrida);

        resultadoCorrida = new JTextArea();
        resultadoCorrida.setBounds(52,120,638,156);
        resultadoCorrida.setEditable(false);

        JScrollPane scroll=new JScrollPane(resultadoCorrida);
        scroll.setBounds(52, 120, 638, 330);
        contentPane.add(scroll);

        porcentaje=new JCheckBox("Data set Prueba: %",false);
        porcentaje.setBounds(52, 56, 165, 23);
        porcentaje.addChangeListener(this);
        contentPane.add(porcentaje);

        separador=new JCheckBox("Separador data set: %",false);
        separador.setBounds(52, 65, 165, 23);
        separador.addChangeListener(this);
        contentPane.add(separador);


        JLabel textoCantidad =new JLabel("Cantidad de corridas: ");
        textoCantidad.setBounds(300, 56, 165, 23);
        contentPane.add(textoCantidad);

        cantidadCorrida = new JTextField();
        cantidadCorrida.setEnabled(true);
        cantidadCorrida.setText("1");
        cantidadCorrida.addKeyListener(new KeyAdapter()
        {
            public void keyTyped(KeyEvent e)
            {
                char caracter = e.getKeyChar();
                if(((caracter < '0') || (caracter > '9') || Integer.parseInt(cantidadCorrida.getText()+caracter)>5000 ) || Integer.parseInt(valorPorcentaje.getText())<=0){
                    e.consume();
                }
            }
        });
        cantidadCorrida.setBounds(467, 56, 40, 23);
        contentPane.add(cantidadCorrida);

        valorPorcentaje = new JTextField();
        valorPorcentaje.setText("10");
        valorPorcentaje.setEnabled(false);
        valorPorcentaje.addKeyListener(new KeyAdapter()
        {
            public void keyTyped(KeyEvent e)
            {
                char caracter = e.getKeyChar();
                if(((caracter < '0') || (caracter > '9') || Integer.parseInt(valorPorcentaje.getText()+caracter)>100 )){
                    e.consume();
                }
            }
        });
        valorPorcentaje.setBounds(218, 56, 29, 23);

        contentPane.add(valorPorcentaje);

        valorSeaprador= new JTextField();
        valorSeaprador.setText("10");
        valorSeaprador.setEnabled(false);
        valorSeaprador.setBounds(218, 65, 29, 23);



        contentPane.add(valorSeaprador);

        btnComenzarCorrida.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                resultadoCorrida.setText("");
                DataSet dataSet =null;
                dataSet = new DataSet();
                Double sumaPorcentajes = 0.0 ;
                Double porcentajeTotal;
                for (Integer indexCorrida = 0; indexCorrida < Integer.parseInt(cantidadCorrida.getText()); indexCorrida++) {
                    boolean ok = dataSet.generarArbol(valorSeaprador.getText(), ruta.getText(), valorPorcentaje.getText());
                    Integer cantidadBien = 0;
                    Double porcentajesBien = 0.0;
                    Integer cantidadTotal=0;
                    Scanner scan = new Scanner(dataSet.getDataSetPrueba()[0]);
                    String headerLine = scan.nextLine();
                    while (scan.hasNext()) {
                        cantidadTotal++;
                        String registroTabla = scan.nextLine();
                        String registroSinComas[] = registroTabla.split(valorSeaprador.getText());
                        AnalizadorPunto punto = new AnalizadorPunto(Double.parseDouble(registroSinComas[0]), Double.parseDouble(registroSinComas[1]), dataSet.getClasesDataSet());
                        String claseSugerida[] = new String[]{punto.clasificarPunto(dataSet.getArbolDecision().get(0))};
                        if ((registroSinComas[2].equals(claseSugerida[0]))) {
                            cantidadBien++;
                        }
                    }
                    porcentajesBien = (double) (cantidadBien * 100) / (cantidadTotal);
                    sumaPorcentajes=sumaPorcentajes +porcentajesBien;
                    Integer corrida = indexCorrida +1;
                    resultadoCorrida.setText(resultadoCorrida.getText() + "Corrida : " + corrida.toString() + '\n');
                    resultadoCorrida.setText(resultadoCorrida.getText() + "Cantidad elementos Data set de prueba :" + cantidadTotal.toString() + '\n');
                    resultadoCorrida.setText(resultadoCorrida.getText() + "Cantidad elementos bien clasidicados:" + cantidadBien.toString() + '\n');
                    resultadoCorrida.setText(resultadoCorrida.getText() + "El porcentaje de acierto en la clasificación es: %" + porcentajesBien.toString() + '\n' );
                    resultadoCorrida.setText(resultadoCorrida.getText() + "------------------------------------------------------------------------------------------" + '\n' );
                }
                    porcentajeTotal= sumaPorcentajes/Double.parseDouble(cantidadCorrida.getText());
                    resultadoCorrida.setText(resultadoCorrida.getText() + "El promedio de todas las corridas es:  %"+ porcentajeTotal.toString());
            }
        });


        btnSeleccionar.addActionListener(new ActionListener(){
            public void actionPerformed (ActionEvent e){


                fc.setAcceptAllFileFilterUsed(false);



                int seleccion=fc.showOpenDialog(contentPane);
                if(seleccion==JFileChooser.APPROVE_OPTION){
                    File fichero=fc.getSelectedFile();
                    fc.setCurrentDirectory(fc.getSelectedFile());
                    try(FileReader fr=new FileReader(fichero)){

                        Scanner scan = new Scanner(new File(fichero.getAbsolutePath()));
                        String headerLine = scan.nextLine();
                        String headers[]  = headerLine.split(valorSeaprador.getText());
                        if (headers.length!=3){
                            JOptionPane.showMessageDialog(null,"El formato del archivo no es el esperado");
                            ruta.setText("");
                        }else{
                            while (scan.hasNext()){
                                String registro = scan.nextLine();
                                String registroSinComas [] = registro.split(valorSeaprador.getText());
                                if (registroSinComas.length!=3){
                                    JOptionPane.showMessageDialog(null,"El formato del archivo no es el esperado");
                                    ruta.setText("");
                                    break;
                                }else{
                                    boolean esNumero=true;
                                    for (int x=0;x < registroSinComas.length - 1;x++){
                                        if (!(isNumeric(registroSinComas[x]))){
                                            JOptionPane.showMessageDialog(null,"El formato del archivo no es el esperado");
                                            esNumero=false;
                                            break;
                                        }
                                    }
                                    if (!esNumero){
                                        ruta.setText("");
                                        fichero =null;
                                        break;
                                    }
                                }
                            }
                            if (fichero!=null){
                                ruta.setText(fichero.getAbsolutePath());
                            }
                        }

                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        });
    }


    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    Corrida frame = new Corrida();
                    frame.setBounds(300,100,800,600);
                    frame.setVisible(true);
                    frame.setTitle("Pruebas de Clasificacion");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }



    @Override
    public void actionPerformed(ActionEvent e) {

    }

    public boolean isNumeric(String cadena) {

        boolean resultado;

        try {
            Double.parseDouble(cadena);
            resultado = true;
        } catch (NumberFormatException excepcion) {
            resultado = false;
        }

        return resultado;
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        if (porcentaje.isSelected()) {
            valorPorcentaje.setEnabled(true);
        } else {
            valorPorcentaje.setText("10");
            valorPorcentaje.setEnabled(false);
        }

    }
}
