package ClasesDataSet;

import java.util.ArrayList;
import java.util.List;

public class Valores {

    private String nombre ;
    private List<String>  classes      = new ArrayList<String>();
    private List<Integer> contadorClases = new ArrayList<Integer>();
    private double entropia = 0.0;
    private boolean pureza;


    public Valores(String valName, String newClass){
        this.nombre = valName;
        this.classes.add(newClass);
        if (!(newClass == null)) {
            this.contadorClases.add(1);
        }else{
            this.contadorClases.add(0);
        }
    }

    public void setPureza(){
        int totalClases = 0;
        for(int i : this.contadorClases) {
            totalClases += i;
        }
        pureza=false;
        for(double d : contadorClases){
            if ((d/totalClases)==1 && pureza==false ){
                pureza=true;
            }

        }

    }



    public void setEntropia(){
        double temp = 0.0;

        int totalClases = 0;
        for(int i : this.contadorClases) {
            totalClases += i;
        }

        for(double d : contadorClases){
            temp = (-1 * (d/totalClases)) * (Math.log((d/totalClases)) / Math.log(2));
            this.entropia += temp;
        }
    }

    public void update(ValorAtributo inValorAtributo) {
        if(this.classes.contains(inValorAtributo.getItClass())){
            this.contadorClases.set(this.classes.indexOf(inValorAtributo.getItClass()),
                    this.contadorClases.get(this.classes.indexOf(inValorAtributo.getItClass())) + 1);
        }
        else{
            this.classes.add(inValorAtributo.getItClass());
            this.contadorClases.add(this.classes.indexOf(inValorAtributo.getItClass()), 1);
        }
    }

    public String getNombre() {
        return nombre;
    }


    public List<String> getClasses() {
        return classes;
    }

    public void setClasses(List<String> classes) {
        this.classes = classes;
    }

    public List<Integer> getContadorClases() {
        return contadorClases;
    }


    public double getEntropia() {
        return entropia;
    }

}
