package ClasesDataSet;

import ClasesArbol.Nodo;
import ClasesArbol.Rama;
import ClasesInterfaz.Linea;
import ClasesInterfaz.Punto;
import ClasesParticion.Particion;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class DataSet {

    private static DefaultTreeModel modelo ;
    private static List<Nodo> arbolDecision = new ArrayList<Nodo>();
    private  static DefaultMutableTreeNode raiz;
    private static List<String []> umbrales = new ArrayList<String []>();
    private static Double mayor_y,mayor_x,menor_y,menor_x = null;
    private static List<Linea> lineasUmbral = new ArrayList<Linea>();
    private static String rutaDataSet;
    private static Integer porcentajePrueba;
    private static String separador;
    private static String [] dataSetPrueba;
    private static String [] dataSetEntrenamiento;
    private static  String [] clasesDataSet = new String [2];


    /**
     * Metodo recursivo donde se realizan las particiones, los casos bases son:
     * 1- El nodo es puro, tiene todas sus ramas puras
     * 3- El nodo es impuro, pero en la particion solo se tienen puntos iguales y clases distintas
     * @param nodo
     * @param dataSet
     * @return
     * @throws IOException
     */
    public static Nodo agregarNodo (Nodo nodo, String[] dataSet) throws IOException {
        if(nodo.esPuro()){
            for (Rama rama:nodo.getRamas()){
                rama.setHoja(rama.getParticion().getClaseParticion());
            }
        }else{
            for(Rama rama: nodo.getRamas()){
                if(!rama.isPureza()){
                    Nodo nodonuevo = analizarDataSet(rama.getParticion().getParticion(),null);
                    if(rama.getParticion().getParticion()[0].equals(nodonuevo.getRamas().get(nodo.getRamas().indexOf(rama)).getParticion().getParticion()[0])){
                        rama.setHoja(clasesPuntosIguales(rama.getParticion()));
                        rama.setPureza(true);
                    }else{
                        rama.setNodos(agregarNodo(nodonuevo,rama.getParticion().getParticion()));
                    }
                }else{
                    if (rama.getParticion().getClaseParticion()!=null) {
                        rama.setHoja(rama.getParticion().getClaseParticion());
                    }
                }
            }
        }
        return nodo;
    }

    /**
     * Metodo que determina si dos puntos son iguales y tienen clases distintas
     * @param particion
     * @return
     */
    public static String clasesPuntosIguales(Particion particion){
        Scanner scanner = new Scanner(particion.getParticion()[0]);

        List<String> clasesDistintas = new ArrayList<>();
        String clase = scanner.nextLine();
        while(scanner.hasNextLine()){
            clase = scanner.nextLine();
            String cl [] = clase.split(separador);
            if (!clasesDistintas.contains(cl[2])){
                clasesDistintas.add(cl[2]);
            }
        }
        return clasesDistintas.get(0) + " - " +clasesDistintas.get(1) ;
    }


    /**
     * Metodo que calcula la entropia del Data set
     */
    public static double calcularEntropiaDataSet(List<Integer> classesCount){
        double entropia = 0.0;
        double entropiaTemporal;

        int totalItems = 0;
        for(int i : classesCount){
            totalItems += i;
        }

        for(double d : classesCount){
            entropiaTemporal = (-1 * (d/totalItems)) * (Math.log((d/totalItems)) / Math.log(2));
            entropia += entropiaTemporal;
        }
        return entropia;
    }


    /**
     * Este metodo hace es el encargado de realizar
     * @param atributos
     * @param contadorClases
     * @param clases
     * @param dataSet
     * @param ruta
     * @return
     * @throws IOException
     */
    public static Nodo algoritmoC45(Atributos[] atributos, List<Integer> contadorClases, List<String> clases, String[] dataSet, String ruta) throws IOException {
        int totalClases = 0;
        for(int i : contadorClases){
            totalClases += i;
        }
        int numeroAtributos = atributos.length;

        //Llamo al metodo que calcula la entropia del data set
        double entD = calcularEntropiaDataSet(contadorClases);

        for (int x = 0 ; x < numeroAtributos ; x++){
            for (int y = 0 ; y < atributos[x].getValores().size() ; y++){
                //calculo el valor de la entropia del valor del atributo
                atributos[x].getValores().get(y).setEntropia();
                atributos[x].getValores().get(y).setPureza();
            }
            //calculo el valor de la entropia del atributo
            atributos[x].setEntropia(entD,totalClases);

        }

        List <Double[]> umbralPorXY = new ArrayList<Double[]>();
        for(int x=0; x < atributos.length;x++){
            List<String[]> atributosOrdeados = orderByValor(atributos,atributos[x]);
            Double[] mejorumbral = calcularMejorUmbral (obtenerUmbrales(atributosOrdeados),atributos[x],entD);
            umbralPorXY.add(mejorumbral);
        }

        List<Particion> particiones;
        Double mejorUmbral;
        int indexAtributo;
        if(umbralPorXY.get(0)[1]>= umbralPorXY.get(1)[1]){
            particiones = filtrarPorUmbral(dataSet,umbralPorXY.get(0)[0],atributos[0],ruta);
            mejorUmbral = umbralPorXY.get(0)[0];
            indexAtributo=0;
            String [] umbralYEje = new String[]{clasesDataSet[0],mejorUmbral.toString()};
            umbrales.add(umbralYEje);

        }else{
           particiones = filtrarPorUmbral(dataSet,umbralPorXY.get(1)[0],atributos[1],ruta);
           mejorUmbral = umbralPorXY.get(1)[0];
           indexAtributo=1;
            String [] umbralYEje = new String[]{clasesDataSet[1],mejorUmbral.toString()};
            umbrales.add(umbralYEje);
        }

        Nodo nodoUmbral = new Nodo (atributos[indexAtributo].getNombre(),particiones,mejorUmbral );
        return nodoUmbral;
    }

    /**
     * Filtra el data set por el umbral que recibe como parametro devolviendo las particiones
     * @param dataSet
     * @param mejorumbral
     * @param atributo
     * @param rutaDataSet
     * @return
     * @throws FileNotFoundException
     */
    private static List<Particion> filtrarPorUmbral(String[] dataSet, Double mejorumbral, Atributos atributo,String rutaDataSet) throws FileNotFoundException {
        Scanner scan;
        if (rutaDataSet!=null){
            scan = new Scanner(new File(dataSet[0]));
        }else{
            scan = new Scanner((dataSet[0]));
        }
        String[] particionPorMayor = new String[dataSet.length];
        String[] particionPorMenor = new String[dataSet.length];
        String headerLine = scan.nextLine();
        String [] headerLineSinComas = headerLine.split(separador);
        int indexAtributo =0;
        for (int x = 0 ; x<headerLineSinComas.length;x++){
            if (headerLineSinComas[x].equals(atributo.getNombre())){
                indexAtributo=x;
            }
        }
        particionPorMayor[0]=headerLine;
        particionPorMayor[0]+= '\n';
        particionPorMenor[0]=headerLine;
        particionPorMenor[0]+= '\n';

        while(scan.hasNextLine()){
            String linea = scan.nextLine();
            String [] lineaSinComas = linea.split(separador);
            if(Double.parseDouble(lineaSinComas[indexAtributo]) < mejorumbral){
                particionPorMenor[0]+=linea;
                particionPorMenor[0]+= '\n';
            }else{
                particionPorMayor[0]+=linea;
                particionPorMayor[0]+= '\n';
            }
        }
        Particion particionMenor =  new Particion("<"+mejorumbral.toString(),particionPorMenor);
        Particion particionMayor =  new Particion(">=" + mejorumbral.toString(), particionPorMayor);
        List<Particion> particiones = new ArrayList<Particion>();
        particiones.add(particionMenor);
        particiones.add(particionMayor);
        return particiones;
    }

    /**
     * Metodo que calcula la entropia y la ganancia del umbral por atributo, devuelve siempre el mejor
     * @param umbralesCandidatos
     * @param atributos
     * @param entD
     * @return
     */
    private static Double[] calcularMejorUmbral(List<Double> umbralesCandidatos, Atributos atributos, Double entD) {
        Double mejorGanancia = 0.0;
        Double mejorUmbral = 0.0;
        for(int indexUmbral=0; indexUmbral < umbralesCandidatos.size();indexUmbral++ ){
            int contadoresPorMenor[]= {0,0,0};
            int contadoresPorMayor[]= {0,0,0};
            String clasesMenor="";
            String clasesMayor="";
            for(int indexItem=0; indexItem < atributos.getValores().size();indexItem++ ){
                if (Double.parseDouble(atributos.getValores().get(indexItem).getNombre())< umbralesCandidatos.get(indexUmbral)){
                    for (int indexClase= 0 ; indexClase < atributos.getValores().get(indexItem).getContadorClases().size();indexClase++){
                        contadoresPorMenor[0]+= atributos.getValores().get(indexItem).getContadorClases().get(indexClase);
                        if (clasesMenor.equals("")){
                            contadoresPorMenor[1]+=atributos.getValores().get(indexItem).getContadorClases().get(indexClase);
                            clasesMenor = atributos.getValores().get(indexItem).getClasses().get(indexClase);
                        }else{
                            if (clasesMenor.equals(atributos.getValores().get(indexItem).getClasses().get(indexClase))){
                                contadoresPorMenor[1]+=atributos.getValores().get(indexItem).getContadorClases().get(indexClase);
                            }else{
                                contadoresPorMenor[2]+=atributos.getValores().get(indexItem).getContadorClases().get(indexClase);
                            }
                        }
                    }
                }else{

                    for (int indexClase= 0 ; indexClase < atributos.getValores().get(indexItem).getContadorClases().size();indexClase++){
                        contadoresPorMayor[0]+=atributos.getValores().get(indexItem).getContadorClases().get(indexClase);;
                        if (clasesMayor.equals("")){
                            contadoresPorMayor[1]++;
                            clasesMayor = atributos.getValores().get(indexItem).getClasses().get(indexClase);
                        }else{
                            if (clasesMayor.equals(atributos.getValores().get(indexItem).getClasses().get(indexClase))){
                                contadoresPorMayor[1]++;
                            }else{
                                contadoresPorMayor[2]++;
                            }
                        }
                    }
                }
            }
            double entropiaMayor=0.0;
            double entropiaMenor=0.0;
            double totalItems;
            double c1 ;
            double c2 ;

            if(contadoresPorMenor[0]!=0){
                totalItems = contadoresPorMenor[0];
                c1 = contadoresPorMenor[1];
                c2 = contadoresPorMenor[2];
                if(c1==0 || c2==0){
                    entropiaMenor=0.0;
                }else{
                    entropiaMenor = (-1 * (c1/totalItems)) * (Math.log((c1/totalItems)) / Math.log(2)) + (-1 * (c2/totalItems)) * (Math.log((c2/totalItems)) / Math.log(2)) ;
                }
            }


            if(contadoresPorMayor[0]!=0){
                totalItems = contadoresPorMayor[0];
                c1 = contadoresPorMayor[1];
                c2 = contadoresPorMayor[2];
                if(c1==0 || c2==0){
                    entropiaMayor=0.0;
                }else{
                    entropiaMayor = (-1 * (c1/totalItems)) * (Math.log((c1/totalItems)) / Math.log(2)) + (-1 * (c2/totalItems)) * (Math.log((c2/totalItems)) / Math.log(2)) ;

                }
            }


            totalItems = contadoresPorMayor[0]+contadoresPorMenor[0];
            double mayores = contadoresPorMayor[0];
            double menores = contadoresPorMenor[0];


            double entropiaUmbral = (mayores/totalItems) * entropiaMayor + (menores/totalItems)*entropiaMenor;
            if(mejorGanancia <  entD - entropiaUmbral){
                mejorGanancia = entD - entropiaUmbral;
                mejorUmbral= umbralesCandidatos.get(indexUmbral);
            }
        }
        Double[] umbralYGanancia = new Double[2];
        umbralYGanancia[0]=mejorUmbral;
        umbralYGanancia[1]=mejorGanancia;

        return umbralYGanancia;
    }


    /**
     * Metodo que recibe el dataset y genera e instancia los atributos
     * @param dataSet
     * @param ruta
     * @return
     * @throws IOException
     */
    public static Nodo analizarDataSet(String[]dataSet, String ruta) throws IOException {
      Scanner scan;
      // Empiezo a recorrer data set. Agarro la primera fila y cada uno de los elementos es el atributo que se va a tratar

      if (ruta != null){
          scan = new Scanner(new File(dataSet[0]));
      }else{
          scan = new Scanner((dataSet[0]));
      }
      String headerLine = scan.nextLine();

      //Quito el saparador del data set
      String headers[]  = headerLine.split(separador);

      // Obtengo la cantidad de atributos que tiene el data set
      int IndexClase    = headers.length - 1;
      int numeroAtributos = headers.length - 1;

      //Creo una arreglo con los atributos del data set, que contiene el nombre del atributo
      Atributos atributos[] = new Atributos[numeroAtributos];
      for(int x = 0; x < numeroAtributos; x++) {
          atributos[x] = new Atributos(headers[x]);
      }


      List<String>  clases      = new ArrayList<String>();
      List<Integer> contadorClases = new ArrayList<Integer>();

      // Recorro el data set hasta la ultima linea
      while(scan.hasNextLine()){
          ValorAtributo data ;
          String linea = scan.nextLine();
          String lineaDataSet[] = linea.split(separador);

          //La primera vez la lista de clases esta vacia, y se carga con el primer valor de la columna de Clases
          if(clases.isEmpty()){
              clases.add(lineaDataSet[IndexClase]);
              contadorClases.add(clases.indexOf(lineaDataSet[IndexClase]), 1);
          }
          else{
              //Si la lista no esta vacia miro si ya existe la clase que estoy analizando.
              //Si no existe aun lo agrego.
              if(!clases.contains(lineaDataSet[IndexClase])){
                  clases.add(lineaDataSet[IndexClase]);
                  contadorClases.add(clases.indexOf(lineaDataSet[IndexClase]), 1);
              }
              //Si ya existe solamente incrementa la cantidad de veces que aparece esa clase
              else {
                  contadorClases.set(clases.indexOf(lineaDataSet[IndexClase]),
                          contadorClases.get(clases.indexOf(lineaDataSet[IndexClase])) + 1);
              }
          }
          // carga por cada item del atributo el valor de la clase condicion
          for(int x = 0; x < numeroAtributos; x++){
              data = new ValorAtributo(lineaDataSet[x], lineaDataSet[IndexClase]);
              atributos[x].insertVal(data);
          }
      }
      Nodo nodo = algoritmoC45(atributos,contadorClases,clases,dataSet,ruta);
      return nodo;

    }


    /**
     * Ordena el el data set por el valor del atributo, teniendo en cuenta que puede venir mas de un clase por valor de atributo
     * @param atributos
     * @param atributo
     * @return
     */
    public static List<String[]> orderByValor (Atributos[] atributos, Atributos atributo){
      String [] resguardoItem1;
      List<String []> atributoOrdenado= new ArrayList<String[]>();
      for(int indexItem=0; indexItem < atributo.getValores().size();indexItem++){
          for (int indexClase=0; indexClase < atributo.getValores().get(indexItem).getClasses().size();indexClase++){

              for (int indexPunto = 0; indexPunto < atributo.getValores().get(indexItem).getContadorClases().get(indexClase);indexPunto++){
                  String [] valorYClase = new String[2];
                  valorYClase[0]=atributo.getValores().get(indexItem).getNombre();
                  valorYClase[1]= atributo.getValores().get(indexItem).getClasses().get(indexClase);
                  atributoOrdenado.add(valorYClase);
              }
          }
      }

      for (int indexValorYClase=0; indexValorYClase < atributoOrdenado.size();indexValorYClase++){
          for (int siguienteValorYClase=indexValorYClase+1; siguienteValorYClase < atributoOrdenado.size();siguienteValorYClase++){
              if (Double.parseDouble(atributoOrdenado.get(indexValorYClase)[0])> Double.parseDouble(atributoOrdenado.get(siguienteValorYClase)[0]) ){
                  resguardoItem1 = atributoOrdenado.get(siguienteValorYClase);
                  atributoOrdenado.set(siguienteValorYClase,atributoOrdenado.get(indexValorYClase));
                  atributoOrdenado.set(indexValorYClase,resguardoItem1);
              }

          }
      }

      //resguadardo mayor valor en "x" y en "y"
      if(atributos[0].getNombre().equals(atributo.getNombre())){
          if (menor_x == null && mayor_x==null){
              menor_x = Double.parseDouble(atributoOrdenado.get(0)[0]);
              mayor_x = Double.parseDouble(atributoOrdenado.get(atributoOrdenado.size()-1)[0]);
          }
      }else{
          if (menor_y == null && mayor_y==null){
              menor_y = Double.parseDouble(atributoOrdenado.get(0)[0]);
              mayor_y = Double.parseDouble(atributoOrdenado.get(atributoOrdenado.size()-1)[0]);
          }
      }
      return atributoOrdenado;
    }

    /**
     * Obtiene los posibles umbrales, compara las clases de los valores adyacentes y si son distintas
     * calcula la media de esos valores.
     * @param atributos
     * @return
     */
    public static List<Double> obtenerUmbrales (List<String[]> atributos){
      List<Double> umbralesCandidatos = new ArrayList<Double>();
        for (int x=0; x < atributos.size()-1;x++){
            if(!(atributos.get(x)[1].equals(atributos.get(x+1)[1]))) {
                    double media = Math.abs(Double.parseDouble(atributos.get(x+1)[0])- Double.parseDouble(atributos.get(x)[0]))/2;
                    umbralesCandidatos.add(Double.parseDouble(atributos.get(x)[0])+ media);
            }
        }
      return umbralesCandidatos;
    }


    /**
     * Metodo que genera la estructura del arbol que se mostrara en la interfaz
     * @param nodo
     * @param raizActual
     */
    public static void agregarArbol(Nodo nodo, DefaultMutableTreeNode raizActual){
        DefaultMutableTreeNode padre = raizActual;
        Nodo nodoActual = nodo;
        for (int x=0; x < nodoActual.getRamas().size(); x++){
                DefaultMutableTreeNode hijo = new DefaultMutableTreeNode(nodoActual.getRamas().get(x).getNombreValor());
                if (!nodoActual.getRamas().get(x).isPureza()){
                    modelo.insertNodeInto(hijo, padre, x);
                    DefaultMutableTreeNode hijoDelHijo = new DefaultMutableTreeNode(nodoActual.getRamas().get(x).getNodos().getNombre());
                    modelo.insertNodeInto(hijoDelHijo, hijo, 0);
                    agregarArbol(nodoActual.getRamas().get(x).getNodos(),hijoDelHijo);
                }else{
                    if(nodoActual.getRamas().get(x).getHoja()!=null){
                        modelo.insertNodeInto(hijo, padre, 0);
                        DefaultMutableTreeNode hoja = new DefaultMutableTreeNode(nodoActual.getRamas().get(x).getHoja());
                        modelo.insertNodeInto(hoja, hijo, 0);
                    }
                }
        }


    }

    /**
     * Metodo que devuelve las lineas de cada particion, estas se dibujaran en el eje cartesiano de la interfaz
     * @param nodo
     * @param x_max
     * @param x_min
     * @param y_max
     * @param y_min
     */
    public void umbralesDataSet(Nodo nodo, Double x_max, Double x_min, Double y_max, Double y_min){

        for(int indexRama=0;indexRama < nodo.getRamas().size();indexRama++) {
            Linea lineaUmbral;
            if (nodo.getRamas().get(indexRama).getNombreValor().contains("<")) {
                if (nodo.getNombre().equals(clasesDataSet[0])) {
                    Punto punto1 = new Punto(Double.parseDouble(nodo.getRamas().get(indexRama).getNombreValor().split("<")[1]), y_max, false);
                    Punto punto2 = new Punto(Double.parseDouble(nodo.getRamas().get(indexRama).getNombreValor().split("<")[1]), y_min, false);
                    lineaUmbral = new Linea(punto1, punto2);
                    if (nodo.getRamas().get(indexRama).getHoja() == null && nodo.getRamas().get(indexRama).getNodos() != null) {
                        umbralesDataSet(nodo.getRamas().get(indexRama).getNodos(), Double.parseDouble(nodo.getRamas().get(indexRama).getNombreValor().split("<")[1]), x_min, y_max, y_min);
                    }
                } else {
                    Punto punto1 = new Punto(x_max, Double.parseDouble(nodo.getRamas().get(indexRama).getNombreValor().split("<")[1]), false);
                    Punto punto2 = new Punto(x_min, Double.parseDouble(nodo.getRamas().get(indexRama).getNombreValor().split("<")[1]), false);
                    lineaUmbral = new Linea(punto1, punto2);
                    if (nodo.getRamas().get(indexRama).getHoja() == null && nodo.getRamas().get(indexRama).getNodos() != null) {
                        umbralesDataSet(nodo.getRamas().get(indexRama).getNodos(), x_max, x_min, Double.parseDouble(nodo.getRamas().get(indexRama).getNombreValor().split("<")[1]), y_min);
                    }
                }
            } else {
                if (nodo.getNombre().equals(clasesDataSet[0])) {
                    Punto punto1 = new Punto(Double.parseDouble(nodo.getRamas().get(indexRama).getNombreValor().split(">=")[1]), y_max, false);
                    Punto punto2 = new Punto(Double.parseDouble(nodo.getRamas().get(indexRama).getNombreValor().split(">=")[1]), y_min, false);
                    lineaUmbral = new Linea(punto1, punto2);
                    if (nodo.getRamas().get(indexRama).getHoja() == null && nodo.getRamas().get(indexRama).getNodos() != null) {
                        umbralesDataSet(nodo.getRamas().get(indexRama).getNodos(), x_max, Double.parseDouble(nodo.getRamas().get(indexRama).getNombreValor().split(">=")[1]), y_max, y_min);
                    }
                } else {
                    Punto punto1 = new Punto(x_max, Double.parseDouble(nodo.getRamas().get(indexRama).getNombreValor().split(">=")[1]), false);
                    Punto punto2 = new Punto(x_min, Double.parseDouble(nodo.getRamas().get(indexRama).getNombreValor().split(">=")[1]), false);
                    lineaUmbral = new Linea(punto1, punto2);
                    if (nodo.getRamas().get(indexRama).getHoja() == null && nodo.getRamas().get(indexRama).getNodos() != null) {
                        umbralesDataSet(nodo.getRamas().get(indexRama).getNodos(), x_max, x_min, y_max, Double.parseDouble(nodo.getRamas().get(indexRama).getNombreValor().split(">=")[1]));
                    }

                }
            }
            lineasUmbral.add(lineaUmbral);
        }
    }

    /**
     * Metodo que es llamado desde la interfaz principal, recibe como parametro la ruta del archivo que se va a procesar
     * y devuelve true si se pudo procesar correctamente el archivo.
     *
     * @param separador
     * @param rutaDataSet
     * @param valorPorcentajeText
     * @return
     */
    public boolean generarArbol(String rutaDataSet, String valorPorcentajeText,String separador) {
        porcentajePrueba=Integer.parseInt(valorPorcentajeText);
         this.separador =separador;
        if (arbolDecision != null) {
            arbolDecision =new ArrayList<Nodo>();
        }

        if(umbrales!=null){
            lineasUmbral = new ArrayList<Linea>();
            menor_x=null;
            menor_y=null;
            mayor_x=null;
            mayor_y = null;
        }

        String dataSet[] = {rutaDataSet};
        try (Scanner scan = new Scanner(new File(dataSet[0]))) {
            String headerLine = scan.nextLine();
            String [] headerLineSinComas = headerLine.split(separador);
            clasesDataSet[0]=headerLineSinComas[0];
            clasesDataSet[1]=headerLineSinComas[1];
            Nodo nodo = null;
            try {
                if (porcentajePrueba > 0 ) {
                    List<String[]> dataSets = sacarPrueba(porcentajePrueba,dataSet);
                    rutaDataSet=null;
                    dataSet=null;
                    nodo = analizarDataSet(dataSets.get(1),null);
                    dataSetPrueba=dataSets.get(0);
                    dataSetEntrenamiento=dataSets.get(1);
                }else{
                    dataSetEntrenamiento=null;
                    dataSetPrueba=null;
                    nodo = analizarDataSet(dataSet,rutaDataSet);
                }

            } catch (IOException e) {
                e.printStackTrace();
            }

            raiz = new DefaultMutableTreeNode(nodo.getNombre());
            modelo = new DefaultTreeModel(raiz);
            try {
                agregarNodo(nodo,dataSet);
            } catch (IOException e) {
                e.printStackTrace();
            }
            arbolDecision.add(nodo);

            agregarArbol(arbolDecision.get(0),raiz);
            umbralesDataSet(arbolDecision.get(0),mayor_x,menor_x,mayor_y,menor_y);
            return  true;

        } catch (FileNotFoundException e) {
            JOptionPane.showMessageDialog(null,"No se encuentra archivo en la ruta indicada");

        }
        return false;
    }

    /**
     * Metodo que genera data set de prueba, a partir del porcentaje ingresado por el usuario
     * @param porcentajePrueb
     * @param dataSet
     * @return
     */
    public List<String[]> sacarPrueba(Integer porcentajePrueb, String[] dataSet) {
        try {
            List<String[]> dataSets = new ArrayList<String[]>();
            Scanner scanner = new Scanner(new File (dataSet[0]));
            String header = scanner.nextLine();
            List<String> dataSetCompleto = new ArrayList<String>();
            int tamañoDataSet=0;
            while (scanner.hasNext()){
                tamañoDataSet++;
                String linea = scanner.nextLine();
                dataSetCompleto.add(linea);
            }


            Integer porcentaje = tamañoDataSet*porcentajePrueb/100;
            Double porcentajeConComa = (double)tamañoDataSet*(double)porcentajePrueb/100;
            if(! (porcentajeConComa % 1 == 0)){
                porcentaje = porcentaje +1;
                if (porcentaje==tamañoDataSet){
                    porcentaje=porcentaje - 1;
                }
            }
            String dataSetEntrenamiento [] = new String[dataSetCompleto.size()];
            String dataSetPrueba []= new String[porcentaje];

            dataSetPrueba[0]=header;
            dataSetPrueba[0]+= '\n';
            dataSetEntrenamiento[0]=header;
            dataSetEntrenamiento[0]+= '\n';


            for (int indexAleatorio=0; indexAleatorio < dataSetPrueba.length;indexAleatorio++){
                int valorRadom = (int) (Math.random()*dataSetCompleto.size());
                dataSetPrueba[0]+=dataSetCompleto.get(valorRadom);
                dataSetCompleto.remove(valorRadom);
                dataSetPrueba[0]+='\n';
            }
            dataSets.add(dataSetPrueba);

            for (int indexCompleto=0;indexCompleto < dataSetCompleto.size();indexCompleto++){
                dataSetEntrenamiento[0]+=dataSetCompleto.get(indexCompleto);
                dataSetEntrenamiento[0]+='\n';
            }
            dataSets.add(dataSetEntrenamiento);
            return dataSets;



        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static DefaultTreeModel getModelo() {
        return modelo;
    }

    public static List<Nodo> getArbolDecision() {
        return arbolDecision;
    }

    public static List<Linea> getLineasUmbral() {
        return lineasUmbral;
    }

    public static String[] getDataSetPrueba() {
        return dataSetPrueba;
    }

    public static String[] getDataSetEntrenamiento() {
        return dataSetEntrenamiento;
    }

    public static String[] getClasesDataSet() {
        return clasesDataSet;
    }

    public static String getSeparador() {
        return separador;
    }
}
