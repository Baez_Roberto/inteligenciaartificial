package ClasesDataSet;

public class ValorAtributo {
    private String nombre;
    private String itClass;


    /**
     *     Aca se instancia el valor propiamente dicho del atributo
      */
    public ValorAtributo(String nombre, String inClass){
        this.nombre = nombre;
        this.itClass =inClass;
    }

    public String getNombre() {
        return nombre;
    }



    public String getItClass() {
        return itClass;
    }

    public void setItClass(String itClass) {
        this.itClass = itClass;
    }
}
