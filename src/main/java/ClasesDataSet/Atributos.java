package ClasesDataSet;

import java.util.ArrayList;
import java.util.List;

/**
 * Representa a cada atributo del data set, esta clase almacenta el nombre del atributo, los valores que puede tomar
 * y sabe como calcular la entropia, ganancia de informacion y el ratio del atributo
 */

public class Atributos {



    private String nombre;
    private List<Valores> valores = new ArrayList<Valores>();
    private double entropia = 0.0;
    private double ganancia = 0.0;
    private double ratio = 0.0;

    /**
     * Constructor de la clase
     * @param nombre
     */
    public Atributos(String nombre){
        this.nombre = nombre;
    }

    /**
     *     Metodo que calgulala entropia del atributo, recibe como parametro la entropia del data set
     *     y el numero de clases que tiene el data set
     */
    public void setEntropia(double IofD, int totalNumClases){
        double denominadorRatio= 0.0;
        int totalValClases = 0;
        for(Valores v : valores){
            for(int i : v.getContadorClases()){
                totalValClases += i;
            }
            denominadorRatio += -(totalValClases/(double)totalNumClases)*(Math.log((totalValClases/(double)totalNumClases)) / Math.log(2));
            this.entropia += (totalValClases/(double)totalNumClases) * v.getEntropia();
            totalValClases = 0;
        }
        this.setGanancia(IofD - this.entropia);
        this.setRatio(this.ganancia/denominadorRatio);

    }

    /**
     * Metodo que agrega un valor al Atributo
     */
    public void insertVal(ValorAtributo inValorAtributo){
        if(this.valores.isEmpty()){
            valores.add(new Valores(inValorAtributo.getNombre(), inValorAtributo.getItClass()));
        }
        else{
            for(Valores v : valores){
                if(v.getNombre().equals(inValorAtributo.getNombre())){
                    v.update(inValorAtributo);
                    return;
                }
            }
            valores.add(new Valores(inValorAtributo.getNombre(), inValorAtributo.getItClass()));
        }
    }

    public double getEntropia() {
        return entropia;
    }

    public double getGanancia() {
        return ganancia;
    }

    public String getNombre() {
        return nombre;
    }

    public List<Valores> getValores() {
        return valores;
    }


    public void setGanancia(double ganancia) {
        this.ganancia = ganancia;
    }

    public double getRatio() {
        return ratio;
    }

    public void setRatio(double ratio) {
        this.ratio = ratio;
    }


}
