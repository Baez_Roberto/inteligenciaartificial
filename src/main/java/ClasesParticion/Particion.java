package ClasesParticion;

import java.util.Scanner;

/**
 * Esta clase almacena la particion que se esta tratando en el algoritmo C4.5
 * la información que contiene un objeto de esta clase, es ela pureza de la partición,
 * si es puro el valor de la clase de la particion
 */

public class Particion {

    private String nombreParticion;
    private String[] particion;
    private boolean purezaParticion;
    private String claseParticion;


    /**
     * Constructor de la clase particion
     * @param nombreParticion
     * @param particion
     */
    public Particion(String nombreParticion, String[] particion) {
        this.nombreParticion=nombreParticion;
        this.particion=particion;
        this.purezaParticion= isPurezaParticion();
    }

    /**
     * Devuelve true si la particion es pura
     * @return
     */
    public boolean esPuro(){
        return purezaParticion;
    }

    /**
     * Este metodo analiza la pureza de la particion
     * @return
     */
    public boolean isPurezaParticion() {
        Scanner scanner = new Scanner(this.particion[0]);
        String headerLine = scanner.nextLine();
        String[] headerLineSinComas =  headerLine.split(";");
        int indexClase =  headerLineSinComas.length - 1;
        String clase = null;
        int contadorClasesDistinta=0;
        while(scanner.hasNextLine()){
            String linea = scanner.nextLine();
            String [] lineaSinComas = linea.split(";");
            if (clase ==  null){
                clase =  lineaSinComas[indexClase];
            }

            if(!(clase.equals(lineaSinComas[indexClase]))) {
                contadorClasesDistinta++;
            }
        }
        if (contadorClasesDistinta==0){
            this.claseParticion = clase;
            return true;
        }
        return false;
    }

    public String getNombreParticion() {
        return nombreParticion;
    }

    public String[] getParticion() {
        return particion;
    }

    public String getClaseParticion() {
        return claseParticion;
    }
}
